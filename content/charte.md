# Charte

Cette charte rassemble les espaces membres du réseau **Coworking Grand Lyon**.

1. Mutualiser un espace de travail d’une superficie minimum de 80 m2 comprenant a minima une salle de réunion, des outils mutualisés, un espace de convivialité dans lequel se trouve un espace adapté à des repas partagés, afin de mettre à disposition un cadre de travail et de partage qui facilite à la fois les collaborations et la convivialité.
2. Organiser au moins 15 ateliers participatifs et des événements pour les coworkers chaque année.
3. Accueillir au minimum 10 membres actifs par mois (objectif à 6 mois pour les nouveaux entrants).
4. Proposer un contexte ouvert et incitatif d’implication des coworkers dans la gouvernance de l’espace de coworking.
5. Les membres du réseau s’engagent à ne pas faire de prosélytisme politique, religieux et syndical.
6. Cultiver un esprit d’entraide en n’accueillant, pour au moins 80% des usagers, que des individus ou équipes composées de 3 personnes maximum d’une même structure.
7. Valoriser à travers différents outils de communication les projets, compétences, savoir-faire et identités des coworkers.
8. Proposer au moins une formule souple permettant de venir « à la carte », sans engagement de durée.
9. Proposer un tarif journalier pour l’espace de coworking et ses services associés inférieur à 40 euros HT.
10. Mettre à disposition des coworkers, dans une logique de transparence, un rapport d’activité comprenant à minima le nombre d’adhérents, le chiffre d’affaires et la répartition des dépenses.
11. Être acteur au sein du collectif Coworking Grand Lyon en participant aux réunions.
12. Co-organiser entre membres actifs du collectif au moins 2 événements par an, ouverts aux coworkers des espaces ainsi qu’à des personnes externes.
13. Afficher la charte Coworking Grand Lyon dans l’espace de coworking.
