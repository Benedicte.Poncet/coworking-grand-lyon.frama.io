---
nom: MIX Coworking
logo: /images/logo-MIX-coworking.png
photo: https://mix-coworking.fr/wp-content/uploads/2019/10/Untitled-design-5.png
location: Tassin/Ecully
---

<!-- logo -->
![Mix Coworking](/images/logo-MIX-coworking.png)

Mix coworking est un espace de travail partagé, situé sur l’Ouest Lyonnais dans le centre de Tassin-la-Demi-Lune et une annexe à Ecully.

Le MIX Coworking c’est plus qu’un bureau, travailler au MIX c’est aussi rejoindre la communauté du MIX.
Le MIX est un lieu de liens avec de la convivialité, un lieu de partage de compétences et d’expérience, un lieu de réseau qui aide à développer son business.

Pourquoi le “MIX” ?  Car ici, on mixe nos talents, nos métiers, nos regards, nos projets, nos réseaux … et de nouvelles idées et opportunités naissent !


# Mix Coworking

<!-- Photo -->
![](https://mix-coworking.fr/wp-content/uploads/2019/07/espace-de-coworking-ouest-lyonnais.jpg)

<!-- Présentation: 1000 caractères max avec les services -->

### MIX Tassin

Dans le centre de Tassin la demi lune, vous pourrez partager un espace de travail professionnel, coopératif et stimulant.
Venez en occasionnel , ou adhérent (plusieurs formules en fonction de vos besoins )
Chacun choisit sa place : le calme de la grande table partagée, l’ambiance chaleureuse du café, l’intimité d’une box individuelle pour se concentrer ou le calme d’une call box pour téléphoner.

Vous pouvez reserver l'une de nos quatre salles de réunion  (1 à 12 personnes) pour recevoir un collaborateur, un client, un prestataire,pour vos recrutements ... pour organiser vos réunions d'équipe et vos formations, pour vos événements professionnels, etc…

Et bien sur un espace café pour échanger et une cuisine partagée pour déjeuner.

De nombreux événements réseaux sont organisés et des ateliers pour enrichir ses compétences et booster son business !

Qui sort s'en sort ! Travailler dans un espace de coworking permet de developper son réseau clients, prescripteurs, prestataires ... et d'avoir de la visibilité!


### MIX Ecully

A Ecully, vous trouverez des bureaux fermés de 2 ou 3 personnes Des bureaux lumineux et chaleureux au design scandinave, clés en main pour vous permettre de vous consacrer à développer votre activité.
Il est possible de louer un bureau pour 1 jour fixe par semaine ou 2 ! 1 bureau dans un espace partagé de 3 bureaux ou 1 bureau individuel qui permet également de recevoir !


## Services
- Domiciliation d'entreprise
- Internet Fibre sécurisé haut débit
- 5 salles de réunion (reservation 30 mn à plusieurs jours)
- Imprimante / scanner A4-A3
- Cuisine partagée
- café/thé
- des nombreux ateliers et événements réseaux
- parking gratuit à proximité

Toutes les charges locatives, électricité, eau, chauffage, assurance des locaux, l’entretien hebdomadaire, sont intégrés.
parking gratuit à proximité

## Localisation

**Mix Tassin** à Tassin la demi lune près de l'horloge
**MIX Ecully** à côté du Botanic

## Plus d'infos

https://www.mix-coworking.fr/

Pour nous contacter :
- contact@mix-coworking.fr
- 07 64 70 20 20 
