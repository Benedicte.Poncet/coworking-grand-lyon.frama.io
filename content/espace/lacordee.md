---
nom: La Cordée
logo: /images/CORDEE_LOGO.png
photo: /images/cordee-liberte-7-1030x688-1030x688.jpg
location: Partout
---

<!-- logo -->
![La Cordée](/images/CORDEE_LOGO.png)
<!-- sous-titre -->
La Cordée c'est dix espaces de coworking dans toute la France.
10 espaces de travail & de partage !
> 🇬🇧 Ten collaborative Working Spaces in France

# La Cordée

<!-- Photo; was https://www.la-cordee.net/wp-content/uploads/Liberte%CC%81-1-845x321-300x114.jpeg but cause horrible error because their workpress does not allow hotlinking -->
![](/images/cordee-liberte-7-1030x688-1030x688.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
La Cordée, ce sont des espaces de travail professionnels, conviviaux et lumineux, où l’on croise des personnes exerçant tous les métiers : consultants, entrepreneurs, commerciaux, développeurs ou encore traducteurs. Ce sont également des espaces d’échange, où l’on peut demander conseil à notre voisin avocat ou trouver un graphiste pour une prestation. Et ce sont enfin des espaces de convivialité, où l’on peut prendre un café en discutant de ses projets et déjeuner tous ensemble le midi autour d’une grande table.

## Services

- Internet Fibre par câble ethernet ou wifi
- Salle de réunion
- Imprimante / scanner / copieur
- Cuisine, micro-onde, frigidaire
- Café à volonté ☕
- Thé dispo en vrac 🌿
- Terrasse l'été pour certains de nos espaces ☀️

## Localisation

Créée en 2011, La Cordée compte aujourd'hui 10 espaces de coworking situés à Lyon, Nantes, Rennes, Paris et Annecy.

## Plus d'infos

Toutes les informations pour nous contacter, ainsi que les photos de nos espaces sont sur notre site : [la-cordee.net](https://www.la-cordee.net/)
