---
nom: CCO Villeurbanne
logo: https://asso.larayonne.org/wp-content/uploads/sites/4/2023/02/CCO-logotype-01-couleur.svg
photo: https://www.cco-villeurbanne.org/app/uploads/2021/08/40-_DSC7104-e1630427453516.jpg
location: Villeurbanne
---

<!-- logo -->
![CCO Villeurbanne](https://asso.larayonne.org/wp-content/uploads/sites/4/2023/02/CCO-logotype-01-couleur.svg)
<!-- sous-titre -->
Espace de coworking associatif, culturel et ESS
> 🇬🇧

# CCO Villeurbanne

<!-- Photo -->
![](https://www.cco-villeurbanne.org/app/uploads/2021/08/40-_DSC7104-e1630427453516.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Le CCO est un laboratoire d’innovation sociale et culturelle. À travers différentes missions, le CCO renforce la capacité de tous les individus à agir, penser et rêver la société :
- accueil et pépinière d’initiatives de l’économie sociale et solidaire
- évènements artistiques et manifestations associatives
- fabrique de l’innovation sociale culturelle et numérique

Depuis plus de 55 ans, le CCO se situe rue Georges Courteline, au nord de Villeurbanne (CCO Jean-Pierre Lachaize). En 2023, le CCO quittera cette rue pour s’installer rue Alfred de Musset à Villeurbanne, dans la friche de l’Autre Soie. Il sera alors renommé « La Rayonne ». Depuis 2017, ce lieu en friche est investi par le CCO et des résidents pour éprouver les futurs usages, faire vivre et connaître le projet dans le cadre d’une occupation temporaire (CCO La Rayonne).
Dans le cadre de son activité pépinière associative et culturelle, le CCO propose sur ses deux lieux différentes ressources et services.

## Services
Au CCO Jean-Pierre Lachaize :
- 6 bureaux individuels et 2 bureaux partagés
- Salles de réunions et salles d’activités (concerts, danse, musique, résidence artistique)
- Photocopieur et accès internet wifi
- Kitchenette partagée et équipée
- Accès au cycle de formation Association Mode d’Emploi

Au CCO La Rayonne :
- 8 espaces de travail individuels ou partagés
- 2 postes coworking fixe et 4 postes coworking nomade
- Deux salles de réunion, un amphithéâtre, un atelier
- Photocopieur et accès internet wifi
- Matériauthèque (outils et matériaux, kit réparation vélo, studio photo)
- Coin déjeuner partagé et équipé
- Parc arboré de 2 ha
- Accès au cycle de formation Association Mode d’Emploi


## Localisation

CCO Jean-Pierre Lachaize : 39 rue Georges Courteline 69100 Villeurbanne

CCO La Rayonne : 24b rue Alfred de Musset 69100 Villeurbanne

Comment venir : https://www.cco-villeurbanne.org/infos-pratiques/comment-venir/

Contact : cco@cco-villeurbanne.org 04 78 93 41 44

https://www.cco-villeurbanne.org/

https://larayonne.org
