---
nom: Espace CoLab
logo: https://espacecolab.adapei69.fr/wp-content/themes/adapeicolab/images/logo.png
photo: https://espacecolab.adapei69.fr/wp-content/uploads/2020/09/060A5848-min-edited-1-1024x575.jpg
---

<!-- logo -->
![](https://espacecolab.adapei69.fr/wp-content/themes/adapeicolab/images/logo.png)

<!-- sous-titre -->
Rejoignez une communauté engagée pour les personnes en situation de handicap !  


# Espace CoLab

<!-- Photo -->
![](https://espacecolab.adapei69.fr/wp-content/uploads/2020/09/060A5848-min-edited-1-1024x575.jpg)


<!-- Présentation: 1000 caractères max avec les services -->
Ouvert en 2019, l'Espace CoLab de [l'Adapei 69](https://www.adapei69.fr/) est un lieu innovant situé au sein de l'Esat Jacques Chavent.

Notre ambition est d’encourager le développement de projets innovants **participant à l’inclusion des personnes en situation de handicap**, à travers une offre double.

**UN ESPACE DE CO-WORKING**
Notre espace de coworking est destiné à accueillir des porteurs de projets tournés vers le développement de services et outils permettant aux personnes en situation de handicap de gagner en autonomie.

**UN LIVING LAB**
Le Living Lab est une méthodologie où les utilisateurs des services développés sont considérés comme des acteurs clés des processus de recherche et d’innovation, pour des résultats au plus proche de leurs besoins et attentes.

## Services 
Les offes de coworking comprennent l'accès :
- aux ateliers et matinales,
- 1 petite salle de réunion,
- une palette de prestations pour améliorer le confort de travail (restauration, …), assurées par les travailleurs de l’ESAT Jacques Chavent
- au copieur et au wifi,
- à un casier personnel,
- à l’intranet Espace CoLab,
- à un jardin aménagé avec des tables et des parasols pour les beaux jours,🌞
- à un terrain de pétanque,
- au parking,🚗
- à un Micro-onde et frigidaire,
- café et thé à volonté ☕

## Localisation

Situé à Gerland, au coeur de la TechSud de Lyon. Nous sommes situés à 8min du Tramaway  T6, arrêt Challemel Lacour Artillerie, le bus 64 s'arrête à 50m et des stations vélo'v se trouvent à proximité.

[33 rue Saint Jean de Dieu, 69007 LYON](https://www.openstreetmap.org/node/6710366211#map=19/45.76471/4.87006)

## Plus d'infos

https://espacecolab.adapei69.fr/
espacecolab@adapei69.fr
