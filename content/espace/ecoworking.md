---
nom: Écoworking
logo: https://i.postimg.cc/pXJsQL4N/logo-ecowo-rgb.png
photo: https://i.postimg.cc/651T7dcy/Open-Space-1er-01-recadrage.png
location: Lyon 1er
---

<!-- logo -->
![Écoworking](https://i.postimg.cc/MHPhz0RS/logo-ecowo-rgb-etire-mid.png)
<!-- sous-titre -->
Votre espace de coworking dans l'hypercentre Lyonnais avec une philosophie [éco-responsable 🌿](https://www.ecoworking.fr/details-notre+fil+vert+partage+de+valeurs+eco-responsable-401)

# Écoworking

<!-- Photo -->
![](https://i.postimg.cc/651T7dcy/Open-Space-1er-01-recadrage.png)

<!-- Présentation: 1000 caractères max avec les services -->
Écoworking ouvre ses portes en 2013 afin de proposer un lieu aux professionnels isolés leur permettant de **mutualiser les énergies**, qu’elles soient **matérielles** (locaux, équipements, ressources) ou **humaines** (compétences, connaissances, motivations).

Notre **communauté** est ainsi **bigarrée** : différentes professions, différents statuts, différents savoir-faire… avec un point commun : une envie d’humain, d’échanges et de soutien.

Nous vous proposons la **location de bureaux dédiés**, en open-space à taille humaine (de 4 à 8 bureaux), ou de **salles de réunion**. Nos offres à la **demi-journée**, **journée** ou **au mois** conviennent aussi bien à la personne de passage qu’à celle qui souhaite s’installer pour une durée indéterminée.

## Services
- Internet Fibre par câble ethernet ou wifi
- [Bureau dédié](https://www.ecoworking.fr/details-formule+resident+un+poste+dedie+dans+un+espace+partage+a+lyon+-+terreaux-391) en open space (+casier fermant à clé)
- [Salles de réunion](https://www.ecoworking.fr/details-location+de+salles+de+reunion+a+la+demi-journee+sur+lyon+1-380) & Cabines d'appels 📞
- Accès illimité 24/7, pour nos résidents
- Domiciliation possible, pour nos résidents
- Espace reprographie - A3 & A4
- Cuisine équipée (plaques de cuisson, four, micro-ondes, frigo)
- Café & Thé à volonté
- Parking à vélo 🚲
- Douche
- [Point de collecte Recyclivre](https://www.recyclivre.com/blog/environnement/point-livres/) 📚

## Localisation

[27 rue Romarin, Lyon](https://www.openstreetmap.org/node/2682137336)

Écoworking se situe à côté de la **place des Terreaux**, célèbre place Lyonnaise, **proche de toutes commodités** et **très bien desservie** par les transports en commun (2 Métro, 7 Bus, 6 stations Velov’, 10-20 min des gares Perrache, St Paul et Part Dieu).

Nos locaux de **450m²** se trouvent dans un immeuble *typique des pentes lyonnaises*, à l’abri des regards : ils sont traversant et **donnent sur des cours intérieures** pour un maximum de **lumière et de tranquillité**.

*Profitez ainsi des commodités de l’hypercentre tout en bénéficiant d’un poste de travail au calme !*

👋 [Visitez nos locaux virtuellement !](https://my.matterport.com/show/?m=uw4AXgKzE1a)

https://www.ecoworking.fr/

contact@ecoworking.fr
