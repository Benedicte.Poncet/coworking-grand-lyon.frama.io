---
nom: Locaux Motiv'
logo: https://locauxmotiv.fr/wp-content/uploads/2021/11/Logo_LM-horizontal-transparent.png
photo: https://locauxmotiv.fr/wp-content/uploads/2021/10/IMG_3407.jpg
location: Lyon 7e
---

<!-- logo -->
![Locaux Motiv'](https://locauxmotiv.fr/wp-content/uploads/2021/11/Logo_LM-horizontal-transparent.png)
<!-- sous-titre -->
Locaux Motiv', un tiers-lieu de l'ESSE en gestion collective
> 🇬🇧 a third-place for the social and ecological economy stakeholders

# Locaux Motiv'

<!-- Photo -->
![](https://locauxmotiv.fr/wp-content/uploads/2021/10/IMG_3407.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Locaux Motiv' est un Tiers-Lieu d'expérimentation, de partage et de diffusion de projet de l'ESSE (Economie Sociale Solidaire et Ecologique). L'association rassemble à la fois entreprises, associations, indépendant·es et personnes individuelles qui se reconnaissent dans des valeurs et des objectifs partagés :
- La gestion, l’animation et la promotion d’un Tiers-Lieu inclusif et basé sur la coopération,
- La coordination, le portage et la diffusion de projets collectifs professionnels,
- La promotion et la contribution à la mise en réseau des acteurs de l’économie sociale, solidaire et écologique.


## Services

- Postes nomades, bureaux partagés, bureaux en propre
- Internet Fibre (via câble ethernet et wifi)
- Salles de réunions
- Espaces de stockage
- Espace reprographie
- Cuisine partagée et équipée
- Terrasse l'été
- Commandes groupées (pain, huiles d'olive, etc.)

## Localisation

Au cœur de la Guillotière (Lyon 7e), sur la place Mazagran.

Infos pratiques sur [locauxmotiv.fr/infos-pratiques/acces/](https://locauxmotiv.fr/infos-pratiques/acces/)

## Plus d'infos

Pour nous rencontrer, inscrivez-vous à un **Jeudi Porte ouverte** ! Rendez-vous sur l'agenda de [locauxmotiv.fr](https://www.locauxmotiv.fr/).
