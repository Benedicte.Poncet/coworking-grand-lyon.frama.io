---
nom: Amabla
logo: https://www.amabla.org/images/logo.png
photo: https://www.amabla.org/images/space/1.jpg
external_url: https://www.amabla.org
location: Villeurbanne
---

<!-- logo -->
![Amabla](https://www.amabla.org/images/logo.png)
<!-- sous-titre -->
Co-working associatif des acteurs du **web** et de la **com'** à Lyon.
> 🇬🇧 Collaborative Working Space in Villeurbanne

# Amabla

<!-- Photo -->
![](https://www.amabla.org/images/space/1.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Dans un local entièrement rénové, l'espace **Amabla** propose un open-space au coeur de **Villeurbanne**.
Pour *tous les besoins*, nous proposons des bureaux fixes ou nomades.

Créée en 2014, l'association **Amabla** est **autogéré** et représente d’abord un **lieu d'échanges et de partage**. Notre espace s'adresse aux acteurs du web et de la com' mais *sans exclusivité*, puisqu'il s'ouvre régulièrement à bien d'autres activités. **Freelances** et **consultants** nomades viennent y **développer** leurs compétences et leurs projets en toute sérénité. Pour nous, le bien-être au travail, est un terreau fertile pour la réussite. Notre espace reste avant tout un **lieu propice au travail, calme et fonctionnel**.

En bonus, l'été, nous disposons d'une **terrasse de rue**, très agréable pour apprécier un **bon café** au soleil! ☀… et un coin de cours intérieur pour cultiver nos boutures ! Car Amabla possède la **fibre "verte" : nous organisons le verdissement de l'espace et encourageons le troc de plantes !

## Services
- Internet Fibre par câble ethernet ou wifi
- 1 salle de réunion
- Imprimante / scanner / copieur
- Cuisine, micro-onde, frigidaire
- [Super café](https://labellebrulerie.fr/) à volonté ☕
- Thé dispo en vrac 🌿
- Pompe à pied & pied d'atelier 🚲
- Terrasse l'été ☀️

## Localisation

Situé à Villeurbanne, dans le quartier du Totem à quelques minutes de la gare de la Part-Dieu, côté Villeurbanne. Nous sommes situés à 7min du métro République, le bus C3 s'arrête à 100m et le quartier est plein de bons restaurants à proximité... :fork_and_knife:

[77 rue d'Alsace, Villeurbanne](https://www.openstreetmap.org/node/6710366211)

## Plus d'infos

[amabla.org](https://www.amabla.org)

contact@amabla.org
