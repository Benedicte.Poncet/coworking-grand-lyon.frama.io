---
nom: La MIETE
logo: https://lamiete.com/wp-content/uploads/2021/03/cropped-cropped-cropped-Logo2019-02-450160.png
photo: https://zupimages.net/up/22/29/n0xb.jpg
location: Villeurbanne
---

<!-- logo -->
![La MIETE](https://lamiete.com/wp-content/uploads/2021/03/cropped-cropped-cropped-Logo2019-02-450160.png)
<!-- sous-titre -->
La MIETE, tiers-lieu associatif à Villeurbanne
>

# La MIETE

<!-- Photo -->
![](https://zupimages.net/up/22/29/n0xb.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
La MIETE (Maison des Initiatives, de l'Engagement, du Troc et de l'Echange) vous accueille dans son tiers-lieu associatif : 900m² de locaux composés de salles d'activités artistiques et manuelles, des bureaux partagés, un café associatif, un fablab et un espace numérique !

Porteur·se de projet de l'ESS en quête d'espace collectif où développer son activité, la MIETE est faite pour vous. Rejoignez un lieu d'expérimentation convivial et partagé.

Nous proposons des bureaux permanents ou bureaux nomades (réservables à la journée), des salles de réunion, une salle de formation, des espaces de convivialité et d'activités.

Convaincu·e ? Envie d'en savoir plus ? Contactez-nous ou venez nous rencontrer, l'équipe vous accueille du mardi au vendredi de 14h à 18h.



## Services
- Internet par wifi
- Salle d'activités et réunions
- Fablab
- Café associatif
- Imprimante / scanner / copieur et petits matériels de bureautique
- Cuisine, micro-onde, frigidaire


## Localisation

La MIETE est au **150 rue du 4 août 1789 à Villeurbanne** dans le centre commercial de la Perralière, proche du **métro Flachet (ligne A)**, arrêt de **bus La Perralière (C26)**. __Une fois devant le centre commercial, prendre la coursive entre le coiffeur et la pharmacie, la MIETE est au fond !__

## Plus d'infos

https://lamiete.com/
Pour nous contacter : contact@lamiete.com
