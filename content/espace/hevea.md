---
nom: Hévéa
logo: /images/hevea-logo.png
photo: /images/HEVEA-lyon-location-bureau-coworking.png
location: Lyon 7eme
---

<!-- logo -->
![Hévéa](/images/hevea-logo.png)
<!-- sous-titre -->
Une cité ETIC responsable responsable à Lyon


# Hévéa

<!-- Photo -->
![](/images/HEVEA-lyon-location-bureau-coworking.png)

<!-- Présentation: 1000 caractères max avec les services -->
En plein cœur de Lyon 7e et à deux pas de la place Jean Macé, une ancienne usine devenue campus universitaire est entrée dans l’économie du XXIème siècle. Depuis 2017, HEVEA est devenue le **lieu totem de l’Économie Sociale & Solidaire à Lyon.** Un tiers-lieu à présent incontournable pour les acteurs et actrices du changement sociétal sur Lyon !



## Services 
- Internet Fibre par câble ethernet ou wifi
- 4 salles de réunion
- Imprimante / scanner / copieur
- 3 Cuisines avec micro-onde et frigidaire
- Un intranet Communautaire
- Une personne à votre écoute au quotidien

## Localisation

Idéalement située proche des gares de Lyon Perrache et de Jean Macé, cette cité du travail éthique de 1 930 m² très facilement accessible propose des **bureaux privatifs, un espace de coworking, des salles de réunion et de conférences à louer ainsi que de nombreux espaces de détente mutualisés.**

2 rue du professeur Zimmermann
69007 Lyon

## Plus d'infos

https://etic.co/location-bureau-coworking-ess/hevea-lyon/

hevea@etic.co
