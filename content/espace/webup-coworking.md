---
nom: Webup Coworking
logo: https://www.webup.space/images/webupspacejauneneutre.png
photo: https://www.webup.space/images/WEBUP_2015_12_07-26.jpg
location: Lyon 7eme
---

<!-- logo -->
![Webup Coworking](https://www.webup.space/images/webupspacejauneneutre.png)
<!-- sous-titre -->
**Coworking à Lyon** à quelques mètres de la place jean Macé 69007


# Webup coworking

<!-- Photo -->
![](https://www.webup.space/images/WEBUP_2015_12_07-26.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Coworking créé en 2015 par 2 anciens coworkeurs de l'Atelier des Médias, notre **espace de coworking** est un plateau de 130m² décoré avec soin accueillant une trentaine de coworkeurs travaillant dans divers domaines d'activité ( édition web, marketing, traduction, ecommerce, blogger ... ). Notre coworking est situé à Lyon dans le 7ème arrondissement juste à côté de la Place Jean Macé.

Notre espace de coworking souhaite favoriser **l'échange et l'entraide entre ses coworkeurs** en instaurant une ambiance conviviale et propice au travail. Afin de faciliter la vie professionnelle de nos membres, de nombreux services sont disponibles gratuitements comme la salle de réunion, la phonebox ou encore l'imprimante. Nous proposons aussi un service de [domiciliation entreprise](https://www.webup.space/domiciliation.php) pour les créateurs ou les entreprises voulant localiser leurs société à Lyon intramuraux.

Notre coworking peut accueil aussi en fin de journée des **événements organisés par des associations locales sur l'entreprenariat**, la création d'entreprise, le marketing, la traduction, les cryptos et bien d'autres. C'est aussi ainsi que nous aidons nos coworkeurs à élargir leur réseau professionnel.

Bref, toute l'équipe du webup coworking a hâte de pouvoir vous accueillir et vous faire découvrir sa communauté d'entrepreneurs, de freelances et de télé travailleurs lyonnais.

## Services de notre coworking
- Internet Fibre par câble ethernet ou wifi
- Une salle de réunion
- Une phone box
- Studio d'enregistrement de podcasts
- Imprimante / scanner / copieur
- Cuisine, micro-onde, frigidaire
- Domicilation d'entreprise
- Parking à vélos, 🚲


## Localisation

Situé à 1 minute à pied de la place Jean Macé dans le 7ème arrondissement de Lyon, notre coworking est déservi par de nombreux moyens de transports ( Gare SNCF, Métro ligne B, Tram T2, Station Velov ). Pour les coworkeurs venant en vélo, nous avons une cours intérieure sécurisée pour les accrocher. Le quartier se situe à 3 arrêts de la Part Dieu et de sa gare SNCF.

Le cadre de vie au alentour est agréable avec de nombreux restaurants et commerces à quelques minutes à pied de notre coworking.:fork_and_knife: Mais vous pourrez aussi flâner ☀️ entre midi et deux au parc Sergent Blandan qui est à 5 minutes à pied !

[52 rue du Colombier 69007 LYON](https://g.page/webup-coworking)

## Plus d'infos

[Webup coworking lyon](https://www.webup.space)
contact@webup.space
