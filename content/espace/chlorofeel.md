---
nom: ChloroFEEL Coworking
logo: https://chlorofeel-coworking.fr/wp-content/uploads/2021/04/cropped-logo-1-1.png
photo: https://chlorofeel-coworking.fr/wp-content/uploads/elementor/thumbs/IMG_2233-scaled-pdje25x5kbkvs5lotqyxdn8y43e2fxp343mhw9m8jo.jpg
location: Meyzieu
---

<!-- logo -->
![ChloroFEEL Coworking](https://chlorofeel-coworking.fr/wp-content/uploads/2021/04/cropped-logo-1-1.png)
<!-- sous-titre -->
ChloroFEEL Coworking, le coworking de l'Est lyonnais
>

# ChloroFEEL coworking

<!-- Photo -->
![](https://chlorofeel-coworking.fr/wp-content/uploads/elementor/thumbs/IMG_2233-scaled-pdje25x5kbkvs5lotqyxdn8y43e2fxp343mhw9m8jo.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Rejoignez nos deux espaces de coworking à MEYZIEU et à GENAS aux portes de Lyon.

Indépendant, étudiant, salarié, à la recherche d’un emploi ou une grande entreprise, vous êtes les bienvenu(e)s !

Openspaces, bureaux individuels, salles de réunion, vous trouverez chez ChloroFEEL toutes les possibilités pour (télé)-travailler à proximité de chez vous ou de vos clients. Plus besoin de prendre les transports en commun ou de subir les embouteillages !

Vous recherchez un espace de travail à la fois calme, convivial et stimulant ? Alors, faites votre première expérience de coworking chez ChloroFEEL !


## Services
- Internet Fibre par câble ethernet ou wifi
- Salle de réunion
- Imprimante / scanner / copieur
- Cuisine, micro-onde, frigidaire
- Coin café/thé

## Localisation

Situé en face de l’arrêt de Tram Meyzieu Gare (à 20 minutes de la Part-Dieu pour l'un et proche de l'aéroport St-Exupéry et d'Eurexpo pour l'autre)

## Plus d'infos

https://www.chlorofeel-coworking.fr
Pour nous contacter : hello@chlorofeel-coworking.fr
